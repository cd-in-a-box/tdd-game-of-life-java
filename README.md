# App

Test Driven Development dojo starting point

## Requirements
- JDK >= 8
- Maven

## Compile application
```
mvn compile
```

## Run unit tests
```
mvn test
```

